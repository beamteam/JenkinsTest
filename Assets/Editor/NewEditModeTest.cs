﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;


public class TestClass
{
    public int A;

    public TestClass(int a)
    {
        A = a;
    }
}

public class NewEditModeTest {

	[Test]
	public void NewEditModeTestSimplePasses()
    {
        var test = new TestClass(5);
        Assert.That(test.A, Is.EqualTo(5));
	}
}
