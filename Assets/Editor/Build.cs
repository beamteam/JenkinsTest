﻿

using UnityEditor;
using UnityEngine;

public class Build : MonoBehaviour
{
    public static string BuildDir = "/mnt/storage/srv/beam/builds/";

    public static void PerformBuild()
    {
        Object o = new Object();
        o?.GetInstanceID();

        string path = BuildDir + "JenkinsTest_" + System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm");
        
        Debug.Log("Build::PerformBuild:: Started build.");
        Debug.Log("Build::PerformBuild:: Path [" + path + "/JenkinsText.exe]");

        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows64);
        
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        buildPlayerOptions.scenes = new [] { "Assets/Scene.unity" };
        buildPlayerOptions.target = BuildTarget.StandaloneWindows64;
        buildPlayerOptions.targetGroup = BuildTargetGroup.Standalone;
        buildPlayerOptions.locationPathName = path + "/JenkinsTest.exe";

        BuildPipeline.BuildPlayer(buildPlayerOptions);
    }
}
